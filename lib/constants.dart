class GeneralErrors {
  static const String INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR';
  static const String REQUEST_TIMEOUT = 'REQUEST_TIMEOUT';
}

const String SECRET_KEY = 'qwerc24214';

const int DB_VERSION = 1;

class VirtualMenu {
  static const String ALL = 'ALL';
  static const String TOP_SELLING = 'TOP_SELLING';
  static const String OTHERS = 'OTHERS';
}
