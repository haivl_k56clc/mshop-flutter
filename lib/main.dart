import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:new_project/themes/theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:new_project/localization.dart';
//hive
import 'package:hive_flutter/hive_flutter.dart';

// screens
import 'package:new_project/screens/home.dart';
import 'package:new_project/screens/splash.dart';
import 'package:new_project/screens/login.dart';
import 'package:new_project/screens/account_info.dart';
import 'package:new_project/screens/product_selector.dart';
import 'package:new_project/screens/product/product_manager.dart';
import 'package:new_project/screens/product/product_info.dart';

void main() async {
  await Hive.initFlutter();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('vi'), // Vietnamese
      ],
      title: 'Mshop Flutter',
      theme: defaultTheme,
      initialRoute: '/',
      routes: {
        '/': (context) => Splash(),
        Login.screenName: (context) => Login(),
        Home.screenName: (context) => Home(),
        AccountInfo.screenName: (context) => AccountInfo(),
        ProductManager.screenName: (context) => ProductManager(),
        ProductSelector.screenName: (context) => ProductSelector(),
        ProductInfo.screenName: (context) => ProductInfo()
      },
    );
  }
}
