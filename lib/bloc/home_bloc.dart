import 'dart:async';
import 'package:new_project/utils/main.dart';
import 'package:dio/dio.dart';
import 'bloc_base.dart';
import 'package:new_project/api/home.dart';

class HomeBloc extends BlocBase {
  StreamController<Map<String, int>> _statisticStreamController =
      StreamController();

  Stream<Map<String, int>> get statisticStream =>
      _statisticStreamController.stream;

  loadStatistic() async {
    try {
      Response statisticResponse = await getStatistic();
      int statusCode = statisticResponse?.statusCode;
      if (statusCode != 200) return;
      Map<String, int> statistic =
          Map<String, int>.from(statisticResponse.data);
      _statisticStreamController.sink.add(statistic);
    } catch (e) {
      print('Statistic Err: ' + e.toString());
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _statisticStreamController.close();
  }
}
