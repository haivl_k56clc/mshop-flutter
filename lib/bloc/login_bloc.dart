import 'dart:async';
import 'package:new_project/utils/main.dart';
import 'package:dio/dio.dart';
import 'package:new_project/db/user.dart';
import 'package:new_project/db/merchant.dart';
import 'bloc_base.dart';
import 'package:new_project/api/auth.dart';

enum LoginEventType { startLogin, loginSuccess, loginFail }

class LoginEvent {
  LoginEvent({this.type, this.message});
  LoginEventType type;
  String message;
}

class LoginBloc extends BlocBase {
  StreamController<LoginEvent> _streamController = StreamController();

  Stream<LoginEvent> get loginStream => _streamController.stream;

  login() async {
    try {
      _streamController.sink.add(LoginEvent(
        type: LoginEventType.startLogin,
      ));
      Response loginRes = await signIn(
          tenant_code: 'geminicoffee',
          userName: '0977865062',
          password: getSha256('123456'));
      Map<String, dynamic> loginResMap;
      if (loginRes?.data is Map<String, dynamic>) {
        loginResMap = loginRes.data;
      }
      // login success
      if (loginResMap != null && loginResMap.containsKey("userId")) {
        User user = User.fromJson(loginResMap);
        List<Map<String, dynamic>> merchantListResponse =
            List<Map<String, dynamic>>.from(loginResMap['listMerchant']);
        List<Merchant> merchantList = merchantListResponse
            .map((item) => Merchant.fromJson(item))
            .toList();
        final userDB = UserDB();
        final merchantDB = MerchantDB();
        final userSaved = await userDB.save(user);
        final merchantSave = await merchantDB.saveList(merchantList);
        // ApiErrorStream.reset();
        _streamController.sink
            .add(LoginEvent(type: LoginEventType.loginSuccess));
      } else if (loginResMap != null && loginResMap.containsKey("code")) {
        _streamController.sink.add(LoginEvent(
            type: LoginEventType.loginFail, message: loginResMap['msg']));
      } else {
        _streamController.sink.add(LoginEvent(type: LoginEventType.loginFail));
      }
    } on DioError catch (err) {
      print('Format exception: ' + err.toString());
      _streamController.sink.add(LoginEvent(type: LoginEventType.loginFail));
    } catch (err) {
      print('General err: ' + err.toString());
      _streamController.sink.add(LoginEvent(type: LoginEventType.loginFail));
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _streamController.close();
  }
}
