import 'dart:async';
import 'bloc_base.dart';
import 'package:new_project/db/product.dart';
import 'package:new_project/api/product.dart';
import 'package:dio/dio.dart';
import 'dart:developer' as developer;
import 'package:new_project/utils/main.dart';
import 'package:new_project/constants.dart';

enum LoginEventType { startLogin, loginSuccess, loginFail }

class LoginEvent {
  LoginEvent({this.type, this.message});
  LoginEventType type;
  String message;
}

class ProductBloc extends BlocBase {
  StreamController<List<Menu>> _listMenuStream = StreamController();
  Stream<List<Menu>> get listMenuStream => _listMenuStream.stream;
  Future<void> loadProduct() async {
    var menuDB = MenuDB();
    List<Menu> menuDBData = await menuDB.getListMenuProduct();
    if (!_listMenuStream.isClosed) {
      _listMenuStream.sink.add(menuDBData);
    }
    Response menuDataResponse = await getMenu();
    if (menuDataResponse?.statusCode == 200) {
      var menuDataParse = List<Map<String, dynamic>>.from(
          chainParse(menuDataResponse.data, ['updated', 'result']) as List);
      if (menuDataParse != null) {
        List<Menu> menuDataApi =
            menuDataParse.map((item) => Menu.fromJson(item)).toList();
        if (!_listMenuStream.isClosed) {
          _listMenuStream.sink.add(menuDataApi);
        }
        developer.log('menuDataApi: ' + menuDataApi?.length?.toString());
        menuDB.saveListMenuProduct(menuDataApi);
      }
    }
  }

  Future<void> loadProductForManager() async {
    var menuDB = MenuDB();
    List<Menu> menuDBData = await menuDB.getListMenuProductForManager();
    if (!_listMenuStream.isClosed) {
      _listMenuStream.sink.add(menuDBData);
    }
    Response menuDataResponse = await getMenu();
    if (menuDataResponse?.statusCode == 200) {
      var menuDataParse = List<Map<String, dynamic>>.from(
          chainParse(menuDataResponse.data, ['updated', 'result']) as List);
      if (menuDataParse != null) {
        List<Menu> menuDataApi =
            menuDataParse.map((item) => Menu.fromJson(item)).toList();
        List<Menu> menuDataForManager = menuDataApi
            .where((item) =>
                item?.merchantMenu?.id != VirtualMenu.ALL &&
                item?.merchantMenu?.id != VirtualMenu.TOP_SELLING)
            .toList();
        if (!_listMenuStream.isClosed) {
          _listMenuStream.sink.add(menuDataForManager);
        }
        developer.log('menuDataApi: ' + menuDataApi?.length?.toString());
        menuDB.saveListMenuProduct(menuDataApi);
      }
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _listMenuStream.close();
  }
}
