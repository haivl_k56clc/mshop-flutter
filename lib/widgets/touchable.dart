import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';

class Touchable extends StatelessWidget {
  Touchable({
    this.key,
    this.onTap,
    this.child,
    this.color = AppColor.white,
  });
  final Key key;
  final Function onTap;
  final Widget child;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColor.white,
      child: InkWell(
        onTap: onTap,
        child: child,
      ),
    );
  }
}
