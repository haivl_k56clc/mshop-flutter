import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';

class TextInput extends StatelessWidget {
  TextInput(
      {this.key,
      this.leftIcon,
      this.placeholder,
      this.onChanged,
      this.controller,
      this.obscureText = false,
      this.textInputAction,
      this.focusNode,
      this.onSubmitted});
  final Key key;
  final String leftIcon;
  final String placeholder;
  final ValueChanged<String> onChanged;
  final TextEditingController controller;
  final bool obscureText;
  final TextInputAction textInputAction;
  final FocusNode focusNode;
  final Function onSubmitted;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColor.black5,
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: 12, vertical: 0), // all(12.0),
        child: Row(
          children: <Widget>[
            Container(
              child: Image.asset(
                'images/' + leftIcon + '.png',
                width: 24,
                height: 24,
                fit: BoxFit.cover,
              ),
              padding: EdgeInsets.only(right: 10.0),
              decoration: BoxDecoration(
                border: Border(
                  right: BorderSide(
                    width: 1,
                    color: AppColor.borderColor,
                  ),
                ),
              ),
            ),
            // TextField(
            //     keyboardAppearance: Brightness.light,
            //     controller: controller,
            //     onChanged: onChanged,
            //     obscureText: obscureText,
            //     decoration: InputDecoration(
            //       hintText: placeholder,
            //       border: InputBorder.none,
            //       contentPadding: EdgeInsets.symmetric(horizontal: 16),
            //     ),
            //     textInputAction: textInputAction,
            //     focusNode: focusNode,
            //     onSubmitted: onSubmitted,
            //   ),
            Expanded(
              child: TextField(
                keyboardAppearance: Brightness.light,
                controller: controller,
                onChanged: onChanged,
                obscureText: obscureText,
                decoration: InputDecoration(
                  hintText: placeholder,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.symmetric(horizontal: 16),
                ),
                textInputAction: textInputAction,
                focusNode: focusNode,
                onSubmitted: onSubmitted,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
