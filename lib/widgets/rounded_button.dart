import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';

class RoundedButton extends StatelessWidget {
  RoundedButton({this.key, this.text, this.onPressed, this.disabled = false});
  final Key key;
  final String text;
  final dynamic onPressed;
  final bool disabled;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 48,
      child: RaisedButton(
        color: AppColor.cerulean,
        splashColor: AppColor.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6.0),
        ),
        child: Text(
          text,
          style: TextStyle(
            color: AppColor.white,
            fontSize: 14,
            fontWeight: FontWeight.bold,
            letterSpacing: -0.24,
          ),
        ),
        onPressed: disabled ? null : onPressed,
      ),
    );
  }
}
