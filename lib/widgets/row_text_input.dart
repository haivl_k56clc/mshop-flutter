import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/themes/text_styles.dart';

class RowTextInput extends StatelessWidget {
  RowTextInput({
    this.key,
    this.label,
    this.placeholder,
    this.onChanged,
    this.controller,
    this.obscureText = false,
  });
  final Key key;
  final String label;
  final String placeholder;
  final ValueChanged<String> onChanged;
  final TextEditingController controller;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColor.white,
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(24, 16, 8, 16),
            width: 118,
            child: Text(label, style: captionTextBlackStyle),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(
                    width: 1,
                    style: BorderStyle.solid,
                    color: AppColor.borderColor2,
                  ),
                ),
              ),
              child: TextField(
                controller: controller,
                onChanged: onChanged,
                obscureText: obscureText,
                decoration: InputDecoration(
                  hintText: placeholder,
                  border: InputBorder.none,
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                ),
                keyboardAppearance: Brightness.light,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
