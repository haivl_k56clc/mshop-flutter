import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/widgets/touchable.dart';

class TagData {
  TagData({this.id, this.label});
  final String id;
  final String label;
}

class MultipleTagSelector extends StatelessWidget {
  MultipleTagSelector(
      {this.key, this.label, this.data, this.values = const [], this.onChange});
  final Key key;
  final String label;
  final List<TagData> data;
  final List<String> values;
  final Function onChange;

  _handlePressTag(item) {
    print('_handlePressTag: ' + item.id);
    int selectedIndex = values != null
        ? values.indexWhere((element) => element == item.id)
        : -1;
    List<String> newValues = List.from(values);
    if (selectedIndex == -1) {
      newValues.add(item.id);
    } else {
      newValues.removeAt(selectedIndex);
    }
    if (onChange != null) {
      onChange(newValues);
    }
  }

  List<Widget> _buildTagList() {
    return data.map((item) {
      bool isActive = values != null
          ? values.indexWhere((element) => element == item.id) > -1
          : false;
      final Color textColor = isActive ? AppColor.primary : AppColor.black50;
      final Color borderColor = isActive ? AppColor.primary : AppColor.black25;
      return Touchable(
        onTap: () => _handlePressTag(item),
        child: Chip(
          label: Text(item.label),
          backgroundColor: AppColor.white,
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(6.0),
              side: BorderSide(color: borderColor)),
          labelStyle: TextStyle(color: textColor),
          labelPadding: EdgeInsets.symmetric(vertical: 4, horizontal: 12),
        ),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: AppColor.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16),
            child: Text(
              label,
              style: AppTextStyle.captionTextBlackStyle,
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
            child: Wrap(
              spacing: 8,
              runSpacing: 8,
              children: _buildTagList(),
            ),
          )
        ],
      ),
    );
  }
}
