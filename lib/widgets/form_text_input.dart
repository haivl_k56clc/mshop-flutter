import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/themes/sizes.dart';

class FormTextInput extends StatelessWidget {
  FormTextInput({
    this.key,
    this.label,
    this.placeholder,
    this.onChanged,
    this.controller,
    this.keyboardType,
    this.textInputAction,
    this.multiline = false,
  });
  final Key key;
  final String label;
  final String placeholder;
  final ValueChanged<String> onChanged;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final bool multiline;

  @override
  Widget build(BuildContext context) {
    final height = multiline
        ? MULTIPLE_LINE_FORM_INPUT_HEIGHT
        : SINGLE_LINE_FORM_INPUT_HEIGHT;
    return Container(
      height: height,
      decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: AppColor.borderColor2,
            ),
          ),
          color: AppColor.white),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 118,
            height: height,
            child: Padding(
              padding: EdgeInsets.fromLTRB(24, 16, 16, 16),
              child: Text(label, style: captionTextBlackStyle),
            ),
          ),
          Container(width: 1, height: height, color: AppColor.borderColor2),
          Expanded(
            child: SizedBox(
              height: height,
              child: TextField(
                controller: controller,
                onChanged: onChanged,
                decoration: InputDecoration(
                  hintText: placeholder,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.symmetric(
                    horizontal: 16,
                    // vertical: 8,
                  ),
                ),
                keyboardAppearance: Brightness.light,
                keyboardType:
                    multiline ? TextInputType.multiline : keyboardType,
                textInputAction: textInputAction,
                maxLines: multiline ? null : 1,
                minLines: 1,
                textAlign: TextAlign.start,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
