import 'dart:async';

import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/widgets/touchable.dart';

class Toolbar extends StatelessWidget {
  Toolbar({this.key, this.title, this.onPressLeft});
  final Key key;
  final String title;
  final Function onPressLeft;

  _handleTap(context) {
    if (onPressLeft != null) {
      onPressLeft();
    } else {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: AppColor.borderColor,
          ),
        ),
        color: AppColor.white,
      ),
      child: Stack(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              // SizedBox(width: 27),
              Touchable(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 12,
                    horizontal: 24,
                  ),
                  child: Image.asset(
                    'images/arrow_longleft.png',
                    width: 24,
                    height: 24,
                    fit: BoxFit.cover,
                  ),
                ),
                onTap: () => _handleTap(context),
              ),
            ],
          ),
          SizedBox(
            height: 48,
            child: Center(
              child: Text(
                title,
                style: defaultToolbarTextStyle,
              ),
            ),
          )
        ],
      ),
    );
  }
}
