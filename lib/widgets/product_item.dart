import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/db/product.dart';
import 'package:new_project/widgets/touchable.dart';

class ProductItem extends StatelessWidget {
  ProductItem({this.onPress, this.key, this.product});
  final Key key;
  final Function onPress;
  final Product product;

  void _handlePress() {
    if (onPress == null) return;
    onPress(product);
  }

  @override
  Widget build(BuildContext context) {
    Widget productAvatar =
        product?.productAvatar != null && product?.productAvatar != ''
            ? ClipRRect(
                child: CachedNetworkImage(
                  imageUrl: product?.productAvatar,
                  placeholder: (context, url) => Image.asset(
                    "images/placeholder.png",
                    width: 48,
                    height: 48,
                    fit: BoxFit.cover,
                  ),
                  errorWidget: (context, url, error) => Image.asset(
                    "images/placeholder.png",
                    width: 48,
                    height: 48,
                    fit: BoxFit.cover,
                  ),
                  width: 48,
                  height: 48,
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(6.0),
              )
            : ClipRRect(
                child: Image.asset(
                  "images/placeholder.png",
                  width: 48,
                  height: 48,
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(6.0),
              );

    // TODO: implement build
    return Touchable(
      onTap: _handlePress,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  productAvatar,
                  SizedBox(width: 16),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          product?.productName ?? '',
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 8),
                        Text(
                          product?.price?.toString(),
                          style:
                              TextStyle(fontSize: 12, color: AppColor.black50),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 1,
              color: AppColor.backgroundColor,
            ),
          ],
        ),
      ),
    );
  }
}
