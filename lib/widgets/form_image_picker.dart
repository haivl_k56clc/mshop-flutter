import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/themes/sizes.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:new_project/widgets/touchable.dart';

class FormImagePicker extends StatelessWidget {
  FormImagePicker({
    this.key,
    this.label,
    this.localFile,
    this.remoteUrl,
    this.onChangeFile,
    this.onDeleteImage,
  });
  final Key key;
  final String label;
  final File localFile;
  final String remoteUrl;
  final Function onChangeFile;
  final Function onDeleteImage;

  _handlePressDelete() {
    print('_handlePressDelete');
    if (onDeleteImage != null) {
      onDeleteImage();
    }
  }

  _handlePressUpdate() async {
    final imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile != null && onChangeFile != null) {
      print('Image file: ' + imageFile.path);
      onChangeFile(imageFile);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: IMAGE_PICKER_FORM_INPUT_HEIGHT,
      decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: AppColor.borderColor2,
            ),
          ),
          color: AppColor.white),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 118,
            height: IMAGE_PICKER_FORM_INPUT_HEIGHT,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(24, 16, 16, 16),
              child: Text(label, style: captionTextBlackStyle),
            ),
          ),
          Container(
              width: 1,
              height: IMAGE_PICKER_FORM_INPUT_HEIGHT,
              color: AppColor.borderColor2),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 14, 16, 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ClipRRect(
                    child: localFile != null
                        ? Image.file(
                            localFile,
                            width: 80,
                            height: 80,
                            fit: BoxFit.cover,
                          )
                        : Image.asset(
                            "images/placeholder.png",
                            width: 80,
                            height: 80,
                            fit: BoxFit.cover,
                          ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  SizedBox(width: 22),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Touchable(
                        onTap: _handlePressDelete,
                        child: Text(
                          'Xoá ảnh',
                          style: Theme.of(context)
                              .textTheme
                              .caption
                              .copyWith(color: AppColor.primary),
                        ),
                      ),
                      SizedBox(height: 16),
                      Touchable(
                        onTap: _handlePressUpdate,
                        child: Text(
                          'Cập nhật ảnh',
                          style: Theme.of(context)
                              .textTheme
                              .caption
                              .copyWith(color: AppColor.primary),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
