import 'package:flutter/material.dart';
import 'package:new_project/themes/images.dart';
import 'package:new_project/themes/colors.dart';

class CheckboxData {
  String id;
  String title;
  CheckboxData({this.id, this.title});
}

class RoundedCheckbox extends StatelessWidget {
  RoundedCheckbox({this.checked, this.key, this.onChange, this.data});
  final bool checked;
  final Key key;
  final CheckboxData data;
  final Function onChange;

  _handleTap() {
    if (onChange != null) {
      onChange(data);
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _handleTap,
      child: Row(
        children: <Widget>[
          checked == true
              ? Image.asset(
                  AppImageAssets.roundedCheckbox,
                  width: 16,
                  height: 16,
                  fit: BoxFit.cover,
                )
              : Container(
                  width: 16,
                  height: 16,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: AppColor.black50,
                      width: 1,
                      style: BorderStyle.solid,
                    ),
                  ),
                ),
          SizedBox(width: 10),
          Text(
            data?.title ?? '',
            style: Theme.of(context).textTheme.caption.copyWith(
                  color: checked == true ? AppColor.primary : AppColor.black50,
                ),
          )
        ],
      ),
    );
  }
}

class HorizontalCheckboxList extends StatelessWidget {
  HorizontalCheckboxList({this.data, this.value, this.onChange});
  List<CheckboxData> data;
  String value;
  Function onChange;

  _handleChange(CheckboxData data) {
    if (onChange == null) return;
    if (data.id == value) return;
    onChange(data);
  }

  List<Widget> _buildListCheckbox() {
    return data
        .map(
          (item) => RoundedCheckbox(
            checked: item.id == value,
            data: item,
            onChange: _handleChange,
          ),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.all(16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: _buildListCheckbox(),
        ),
      ),
    );
  }
}
