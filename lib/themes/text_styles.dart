import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';

class AppTextStyle {
  static const TextStyle defaultTextStyle = TextStyle(
    fontSize: 14,
    letterSpacing: -0.24,
    color: AppColor.black85,
    fontWeight: FontWeight.normal,
  );
  static const TextStyle defaultTextMediumStyle = TextStyle(
    fontSize: 14,
    letterSpacing: -0.24,
    color: AppColor.black85,
    fontFamily: 'SFProText-Medium',
    fontWeight: FontWeight.w600,
  );

  static const TextStyle captionTextStyle = TextStyle(
    fontSize: 12,
    letterSpacing: -0.24,
    color: AppColor.black50,
  );

  static const TextStyle buttonActiveTextStyle = TextStyle(
      fontSize: 12,
      letterSpacing: -0.24,
      color: AppColor.white,
      fontWeight: FontWeight.bold);

  static const TextStyle captionTextBlackStyle = TextStyle(
    fontSize: 12,
    letterSpacing: -0.24,
    color: AppColor.black85,
  );

  static const TextStyle defaultBoldTextStyle = TextStyle(
      fontSize: 14,
      letterSpacing: -0.24,
      color: AppColor.black85,
      fontWeight: FontWeight.bold);

  static const TextStyle dashboardInfoTextStyle = TextStyle(
    fontWeight: FontWeight.bold,
    color: AppColor.white,
    letterSpacing: -0.24,
    fontSize: 30,
    height: 1.33333,
  );

  final dashboardLabelTextStyle = TextStyle(
    fontWeight: FontWeight.bold,
    color: AppColor.babyBlue,
    letterSpacing: -0.18,
    fontSize: 12,
  );

  final defaultToolbarTextStyle = TextStyle(
      color: AppColor.black85, letterSpacing: 0, fontSize: 16, height: 1.5);
}

final defaultTextStyle = TextStyle(
  fontSize: 14,
  letterSpacing: -0.24,
  color: AppColor.black85,
  fontWeight: FontWeight.normal,
);

final defaultTextMediumStyle = TextStyle(
  fontSize: 14,
  letterSpacing: -0.24,
  color: AppColor.black85,
  fontFamily: 'SFProText-Medium',
  fontWeight: FontWeight.w600,
);

final captionTextStyle = TextStyle(
  fontSize: 12,
  letterSpacing: -0.24,
  color: AppColor.black50,
);

final buttonActiveTextStyle = TextStyle(
    fontSize: 12,
    letterSpacing: -0.24,
    color: AppColor.white,
    fontWeight: FontWeight.bold);

final captionTextBlackStyle = TextStyle(
  fontSize: 12,
  letterSpacing: -0.24,
  color: AppColor.black85,
);

final defaultBoldTextStyle = TextStyle(
    fontSize: 14,
    letterSpacing: -0.24,
    color: AppColor.black85,
    fontWeight: FontWeight.bold);

final dashboardInfoTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  color: AppColor.white,
  letterSpacing: -0.24,
  fontSize: 30,
  height: 1.33333,
);

final dashboardLabelTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  color: AppColor.babyBlue,
  letterSpacing: -0.18,
  fontSize: 12,
);

final defaultToolbarTextStyle = TextStyle(
    color: AppColor.black85, letterSpacing: 0, fontSize: 16, height: 1.5);
