// final homeTab = (_selectedIndex == 0)
//     ? 'images/bottombar_home_active.png'
//     : 'images/bottombar_home.png';
// final orderTab = (_selectedIndex == 1)
//     ? 'images/bottombar_order_active.png'
//     : 'images/bottombar_order.png';
// final reportTab = (_selectedIndex == 2)
//     ? 'images/bottombar_report_active.png'
//     : 'images/bottombar_report.png';
// final configTab = (_selectedIndex == 3)
//     ? 'images/bottombar_config_active.png'
//     : 'images/bottombar_config.png';

class AppImageAssets {
  static String bottomHomeActive = 'images/bottombar_home_active.png';
  static String bottomHome = 'images/bottombar_home.png';
  static String bottomOrderActive = 'images/bottombar_order_active.png';
  static String bottomOrder = 'images/bottombar_order.png';
  static String bottomReportActive = 'images/bottombar_report_active.png';
  static String bottomReport = 'images/bottombar_report.png';
  static String bottomConfigActive = 'images/bottombar_config_active.png';
  static String bottomConfig = 'images/bottombar_config.png';
  static String roundedCheckbox = 'images/round_checkbox.png';
}
