import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';

final defaultTheme = ThemeData(
  fontFamily: "SFProText",
  primaryColorDark: AppColor.primary,
  primaryColor: AppColor.primary,
);
