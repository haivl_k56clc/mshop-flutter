import 'package:flutter/material.dart';

class AppColor {
  static const Color primary = const Color(0xff047cd7);
  static const Color cerulean = const Color(0xff047cd7);
  static const Color greenishTeal = const Color(0xff34c47c);
  static const Color black5 = const Color(0x0c000000);
  static const Color black25 = const Color(0x3f000000);
  static const Color black50 = const Color.fromARGB(127, 0, 0, 0);
  static const Color black85 = const Color.fromARGB(217, 0, 0, 0);
  static const Color black = const Color(0xff000000);
  static const Color dark = const Color(0xff1a1824);
  static const Color redPink = const Color(0xffff2650);
  static const Color paleLilac = const Color(0xffeaeaeb);
  static const Color violetBlue = const Color(0xff410adf);
  static const Color iceBlue = const Color(0xffeff0f1);

  static const Color yellowOrange = const Color(0xfff7b500);
  static const Color babyBlue = const Color(0xff9cd4ff);
  static const Color white = const Color(0xffffffff);
  static const Color white85 = const Color.fromARGB(217, 255, 255, 255);
  static const Color textBlack = const Color.fromARGB(217, 0, 0, 0);

  static const Color backgroundColor = const Color.fromARGB(255, 246, 246, 246);
  static const Color borderColor = const Color.fromARGB(26, 0, 0, 0);
  static const Color borderColor2 = const Color(0xfff2f2f2);
  static const Color babyBlue10 = const Color.fromARGB(26, 156, 212, 255);
}
