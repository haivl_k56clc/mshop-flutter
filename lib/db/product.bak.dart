// // "id" -> "TOP_SELLING"
// // "name" -> "Bán chạy"

// import './base_dao.dart';
// import './base_entity.dart';

// final String tableMenu = 'menu';
// final String columnMenuId = 'id';
// final String columnMenuName = 'name';

// final String createMenuTableQuery = '''
//         CREATE TABLE $tableMenu (
//           $columnMenuId TEXT primary key,
//           $columnMenuName TEXT
//         )
// ''';

// class Menu extends BaseEntity {
//   String id;
//   String name;
//   List<Product> listSaleProductDiscountDto;

//   Menu.fromJson(Map<String, dynamic> map) : super.fromJson(map);

//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{
//       columnMenuId: id,
//       columnMenuName: name,
//     };
//     return map;
//   }

//   void fromJson(Map<String, dynamic> map) {
//     id = map[columnMenuId];
//     name = map[columnMenuName];
//     listSaleProductDiscountDto = map['listSaleProductDiscountDto'];
//   }
// }

// class MenuDB extends BaseDB<Menu> {
//   @override
//   String getTableName() {
//     return tableMenu;
//   }

//   @override
//   Menu fromJson(Map<String, dynamic> row) {
//     return Menu.fromJson(row);
//   }

//   @override
//   List<Menu> getAll() {
//     // String query = '''
//     //   SELECT m.id as menuId, m.name as menuName,
//     //     p.id as productId,
//     //   FROM menu m
//     //   INNTER JOIN product p
//     //   ON menu.id = product.menuId;
//     // ''';
//   }
// }

// // 0:"id" -> "1720160010031908211020071281081"
// // 1:"productName" -> "san pham tao boi pentestshop"
// // 2:"orinalPrice" -> 0
// // 3:"price" -> 100000
// // 4:"unit" -> "0"
// // 5:"quantity" -> 0
// // 6:"productAvatar" -> ""
// // 7:"productCategory" -> 0
// // 8:"merchantId" -> null
// // 9:"listProductMedia" -> null
// // 10:"productCode" -> ""
// // 11:"status" -> 0
// // 12:"description" -> ""
// // 13:"printerId" -> null
// // 14:"listProductMenu" -> null

// final String tableProduct = 'product';
// final String columnProductId = 'id';
// final String columnProductName = 'productName';
// final String columnProductOriginPrice = 'orinalPrice';
// final String columnProductPrice = 'price';
// final String columnProductQuantity = 'quantity';
// final String columnProductAvatar = 'productAvatar';
// final String columnProductDescription = 'description';
// final String columnProductPrinterId = 'printerId';
// final String columnProductCode = 'productCode';
// final String columnProductStatus = 'status';

// final String createProductTableQuery = '''
//         CREATE TABLE $tableProduct (
//           $columnProductId TEXT primary key,
//           $columnProductName TEXT,
//           $columnProductOriginPrice REAL,
//           $columnProductPrice REAL,
//           $columnProductQuantity INT,
//           $columnProductAvatar TEXT,
//           $columnProductDescription TEXT,
//           $columnProductPrinterId TEXT,
//           $columnProductCode TEXT,
//           $columnProductStatus INT
//         )
// ''';

// class Product extends BaseEntity {
//   String id;
//   String productName;
//   String orinalPrice;
//   String price;
//   String quantity;
//   String productAvatar;
//   String description;
//   String printerId;
//   String productCode;
//   String status;

//   Product.fromJson(Map<String, dynamic> map) : super.fromJson(map);

//   Map<String, dynamic> toJson() {
//     var map = <String, dynamic>{
//       columnProductId: id,
//       columnProductName: productName,
//       columnProductOriginPrice: orinalPrice,
//       columnProductPrice: price,
//       columnProductQuantity: quantity,
//       columnProductAvatar: productAvatar,
//       columnProductDescription: description,
//       columnProductPrinterId: printerId,
//       columnProductCode: productCode,
//       columnProductStatus: status,
//     };
//     return map;
//   }

//   void fromJson(Map<String, dynamic> map) {
//     id = map[columnProductId];
//     productName = map[columnProductName];
//     orinalPrice = map[columnProductOriginPrice];
//     price = map[columnProductPrice];
//     quantity = map[columnProductQuantity];
//     productAvatar = map[columnProductAvatar];
//     description = map[columnProductDescription];
//     printerId = map[columnProductPrinterId];
//     productCode = map[columnProductCode];
//     status = map[columnProductStatus];
//   }
// }

// class ProductDB extends BaseDB<Product> {
//   @override
//   String getTableName() {
//     return tableProduct;
//   }

//   @override
//   Product fromJson(Map<String, dynamic> row) {
//     return Product.fromJson(row);
//   }
// }
