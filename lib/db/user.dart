import './base_dao.dart';
import './base_entity.dart';
import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';

final String tableUser = 'user';
final String columnUserId = 'userId';
final String columnUserName = 'userName';
final String columnName = 'name';
final String columnPhone = 'phone';
final String columnBankAccount = 'bankAccount';
final String columnAccessToken = 'accessToken';
final String columnAvatar = 'avatar';
final String columnTenantCode = 'tenantCode';

final String createUserTableQuery = '''
        CREATE TABLE $tableUser ( 
          $columnUserId TEXT primary key, 
          $columnUserName TEXT,
          $columnName TEXT,
          $columnPhone TEXT,
          $columnBankAccount TEXT,
          $columnAccessToken TEXT,
          $columnAvatar TEXT,
          $columnTenantCode TEXT
        )
''';

@JsonSerializable()
class User extends BaseEntity {
  String userId;
  String userName;
  String name;
  String phone;
  String bankAccount;
  String accessToken;
  String avatar;
  String tenantCode;

  User(
      {this.userId,
      this.userName,
      this.name,
      this.phone,
      this.bankAccount,
      this.accessToken,
      this.avatar,
      this.tenantCode});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  // User.fromJson(Map<String, dynamic> map) {
  //   fromJson(map);
  // }

  // Map<String, dynamic> toJson() {
  //   var map = <String, dynamic>{
  //     columnUserId: userId,
  //     columnUserName: userName,
  //     columnName: name,
  //     columnPhone: phone,
  //     columnBankAccount: bankAccount,
  //     columnAccessToken: accessToken,
  //     columnAvatar: avatar,
  //     columnTenantCode: tenantCode
  //   };
  //   return map;
  // }

  // void fromJson(Map<String, dynamic> map) {
  //   userId = map[columnUserId];
  //   userName = map[columnUserName];
  //   name = map[columnName];
  //   phone = map[columnPhone];
  //   bankAccount = map[columnBankAccount];
  //   accessToken = map[columnAccessToken];
  //   avatar = map[columnAvatar];
  //   tenantCode = map[columnTenantCode];
  // }
}

class UserDB extends BaseDB<User> {
  static User _instance;
  @override
  String getTableName() {
    return tableUser;
  }

  @override
  User fromJson(Map<String, dynamic> row) {
    return User.fromJson(row);
  }

  static Future<User> getUserCached() async {
    if (_instance == null) {
      UserDB userDB = UserDB();
      _instance = await userDB.getOne();
    }
    return _instance;
  }
}
