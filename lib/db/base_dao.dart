import './database_helper.dart';
import './base_entity.dart';
import 'package:sqflite/sql.dart';

abstract class BaseDB<T extends BaseEntity> {
  String getTableName();
  T fromJson(Map<String, dynamic> row);

  Future<T> save(T data) async {
    final dbHelper = DbHelper();
    var db = await dbHelper.instance;
    await db.delete(getTableName());
    await db.insert(getTableName(), data.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return data;
  }

  Future<List<T>> saveList(List<T> data) async {
    final dbHelper = DbHelper();
    var db = await dbHelper.instance;
    await db.delete(getTableName());
    var batch = db.batch();
    data.forEach((item) {
      db.insert(getTableName(), item.toJson(),
          conflictAlgorithm: ConflictAlgorithm.replace);
    });
    await batch.commit();
    return data;
  }

  Future<T> getOne() async {
    final dbHelper = DbHelper();
    var db = await dbHelper.instance;
    List<Map> maps = await db.query(getTableName());
    if (maps.length > 0) {
      return fromJson(maps.first);
    }
    return null;
  }

  Future<List<T>> getAll() async {
    final dbHelper = DbHelper();
    var db = await dbHelper.instance;
    List<Map> list = await db.query(getTableName());
    if (list == null) return null;
    return list.map((item) {
      return fromJson(item);
    }).toList();
  }

  Future<List<T>> getByListField(
      List<dynamic> fieldValues, String fieldName) async {
    final dbHelper = DbHelper();
    var db = await dbHelper.instance;
    List<Map> list = await db.query(
      getTableName(),
      where: "$fieldName in (${fieldValues.map((item) => '?').join(',')})",
      whereArgs: fieldValues,
    );
    if (list == null) return null;
    return list.map((item) {
      return fromJson(item);
    }).toList();
  }

  Future drop() async {
    final dbHelper = DbHelper();
    var db = await dbHelper.instance;
    await db.execute("DROP TABLE IF EXISTS ${getTableName()}");
  }

  Future deleteAll() async {
    final dbHelper = DbHelper();
    var db = await dbHelper.instance;
    await db.delete(getTableName());
  }
}
