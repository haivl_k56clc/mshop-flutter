// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MenuProductMapping _$MenuProductMappingFromJson(Map<String, dynamic> json) {
  return MenuProductMapping(
    menuId: json['menuId'] as String,
    productId: json['productId'] as String,
  );
}

Map<String, dynamic> _$MenuProductMappingToJson(MenuProductMapping instance) =>
    <String, dynamic>{
      'menuId': instance.menuId,
      'productId': instance.productId,
    };

MerchantMenu _$MerchantMenuFromJson(Map<String, dynamic> json) {
  return MerchantMenu(
    id: json['id'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$MerchantMenuToJson(MerchantMenu instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

Menu _$MenuFromJson(Map<String, dynamic> json) {
  return Menu(
    merchantMenu: json['merchantMenu'] == null
        ? null
        : MerchantMenu.fromJson(json['merchantMenu'] as Map<String, dynamic>),
    listSaleProductDiscountDto: (json['listSaleProductDiscountDto'] as List)
        ?.map((e) =>
            e == null ? null : Product.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$MenuToJson(Menu instance) => <String, dynamic>{
      'merchantMenu': instance.merchantMenu,
      'listSaleProductDiscountDto': instance.listSaleProductDiscountDto,
    };

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    id: json['id'] as String,
    productName: json['productName'] as String,
    orinalPrice: json['orinalPrice'] as int,
    price: json['price'] as int,
    quantity: json['quantity'] as int,
    productAvatar: json['productAvatar'] as String,
    description: json['description'] as String,
    printerId: json['printerId'] as String,
    productCode: json['productCode'] as String,
    status: json['status'] as int,
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'productName': instance.productName,
      'orinalPrice': instance.orinalPrice,
      'price': instance.price,
      'quantity': instance.quantity,
      'productAvatar': instance.productAvatar,
      'description': instance.description,
      'printerId': instance.printerId,
      'productCode': instance.productCode,
      'status': instance.status,
    };
