import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import './user.dart';
import './merchant.dart';
import './product.dart';
import 'package:new_project/constants.dart';

class DbHelper {
  static final DbHelper _instance = DbHelper._();
  static Database _database;
  DbHelper._();

  factory DbHelper() {
    return _instance;
  }

  _onCreate(Database db, int version) async {
    db.transaction((tran) async {
      await db.execute(createUserTableQuery);
      await db.execute(createMerchantTableQuery);
      await db.execute(createMenuTableQuery);
      await db.execute(createProductTableQuery);
      await db.execute(createMenuProductMappingTableQuery);
    });
  }

  _onUpgrade(Database db, int version) async {
    // TODO: implement
  }

  Future<Database> get instance async {
    if (_database != null) {
      return _database;
    }
    _database = await init();
    return _database;
  }

  Future<Database> init() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String dbPath = join(directory.path, 'database.db');

    var database =
        openDatabase(dbPath, version: DB_VERSION, onCreate: _onCreate);
    return database;
  }
}
