import 'package:new_project/constants.dart';

import 'base_entity.dart';
import 'base_dao.dart';
import 'package:json_annotation/json_annotation.dart';
import 'database_helper.dart';
part 'product.g.dart';

final String tableMenuProductMapping = 'menuProductMapping';
final String columnMenuProductMappingMenuId = 'menuId';
final String columnMenuProductMappingProductId = 'productId';

final String createMenuProductMappingTableQuery = '''
        CREATE TABLE $tableMenuProductMapping ( 
          $columnMenuProductMappingMenuId TEXT, 
          $columnMenuProductMappingProductId TEXT
        )
''';

@JsonSerializable()
class MenuProductMapping extends BaseEntity {
  final String menuId;
  final String productId;

  MenuProductMapping({this.menuId, this.productId});
  factory MenuProductMapping.fromJson(Map<String, dynamic> json) =>
      _$MenuProductMappingFromJson(json);
  Map<String, dynamic> toJson() => _$MenuProductMappingToJson(this);
}

class MenuProductMappingDB extends BaseDB<MenuProductMapping> {
  @override
  MenuProductMapping fromJson(Map<String, dynamic> row) {
    return MenuProductMapping.fromJson(row);
  }

  @override
  String getTableName() {
    return tableMenuProductMapping;
  }
}

@JsonSerializable()
class MerchantMenu extends BaseEntity {
  final String id;
  final String name;

  MerchantMenu({this.id, this.name});
  factory MerchantMenu.fromJson(Map<String, dynamic> json) =>
      _$MerchantMenuFromJson(json);
  Map<String, dynamic> toJson() => _$MerchantMenuToJson(this);
}

final String tableMenu = 'menu';
final String columnMenuId = 'id';
final String columnMenuName = 'name';

final String createMenuTableQuery = '''
        CREATE TABLE $tableMenu ( 
          $columnMenuId TEXT primary key, 
          $columnMenuName TEXT
        )
''';

@JsonSerializable()
class Menu extends BaseEntity {
  final MerchantMenu merchantMenu;
  final List<Product> listSaleProductDiscountDto;
  Menu({this.merchantMenu, this.listSaleProductDiscountDto});
  factory Menu.fromJson(Map<String, dynamic> json) => _$MenuFromJson(json);
  Map<String, dynamic> toJson() => _$MenuToJson(this);
}

final String tableProduct = 'product';
final String columnProductId = 'id';
final String columnProductName = 'productName';
final String columnProductOriginPrice = 'orinalPrice';
final String columnProductPrice = 'price';
final String columnProductQuantity = 'quantity';
final String columnProductAvatar = 'productAvatar';
final String columnProductDescription = 'description';
final String columnProductPrinterId = 'printerId';
final String columnProductCode = 'productCode';
final String columnProductStatus = 'status';
final String columnProductMenuId = 'menuId';

final String createProductTableQuery = '''
        CREATE TABLE $tableProduct ( 
          $columnProductId TEXT primary key, 
          $columnProductName TEXT,
          $columnProductOriginPrice INT,
          $columnProductPrice INT,
          $columnProductQuantity INT,
          $columnProductAvatar TEXT,
          $columnProductDescription TEXT,
          $columnProductPrinterId TEXT,
          $columnProductCode TEXT,
          $columnProductStatus INT,
          $columnProductMenuId TEXT
        )
''';

@JsonSerializable()
class Product extends BaseEntity {
  String id;
  String productName;
  int orinalPrice;
  int price;
  int quantity;
  String productAvatar;
  String description;
  String printerId;
  String productCode;
  int status;
  Product({
    this.id,
    this.productName,
    this.orinalPrice,
    this.price,
    this.quantity,
    this.productAvatar,
    this.description,
    this.printerId,
    this.productCode,
    this.status,
  });
  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}

class MenuDB extends BaseDB<MerchantMenu> {
  @override
  String getTableName() {
    return tableMenu;
  }

  @override
  MerchantMenu fromJson(Map<String, dynamic> row) {
    return MerchantMenu.fromJson(row);
  }

  Future<List<Menu>> saveListMenuProduct(List<Menu> data) async {
    List<MerchantMenu> listMerchantMenu = [];
    List<Product> listProduct = [];
    List<MenuProductMapping> listMenuProductMapping = [];
    data.forEach((menuItem) {
      listMerchantMenu.add(menuItem.merchantMenu);
      menuItem.listSaleProductDiscountDto.forEach((productItem) {
        String menuId = menuItem.merchantMenu.id;
        listProduct.add(productItem);
        MenuProductMapping mapping =
            MenuProductMapping(menuId: menuId, productId: productItem.id);
        listMenuProductMapping.add(mapping);
      });
    });
    ProductDB productDB = ProductDB();
    MenuProductMappingDB mappingDB = MenuProductMappingDB();
    this.deleteAll();
    productDB.deleteAll();
    mappingDB.deleteAll();
    await this.saveList(listMerchantMenu);
    await productDB.saveList(listProduct);
    await mappingDB.saveList(listMenuProductMapping);
    return data;
  }

  Future<List<Menu>> getListMenuProduct() async {
    final dbHelper = DbHelper();
    var db = await dbHelper.instance;
    List<Map<String, dynamic>> flatResult = await db.rawQuery("""
        SELECT 
        m.id, m.name, mpp.productId,
        p.productName, p.orinalPrice, p.price,
        p.quantity, p.productAvatar, p.description,
        p.productCode, p.status
        FROM
        $tableMenu m
        LEFT JOIN
        $tableMenuProductMapping mpp
        ON m.id = mpp.menuId
        LEFT JOIN 
        $tableProduct p
        ON mpp.productId = p.id
    """);
    List<Menu> result = [];

    Map<String, Menu> resultMap = Map<String, Menu>();
    flatResult.forEach((item) {
      Product product = Product(
          id: item['id'],
          productName: item['productName'],
          orinalPrice: item['orinalPrice'],
          price: item['price'],
          quantity: item['quantity'],
          productAvatar: item['productAvatar'],
          description: item['description'],
          printerId: item['printerId'],
          productCode: item['productCode'],
          status: item['status']);
      if (resultMap[item['id']] == null) {
        List<Product> productList = [product];
        resultMap[item['id']] = Menu(
            merchantMenu: MerchantMenu(id: item['id'], name: item['name']),
            listSaleProductDiscountDto: productList);
      } else {
        resultMap[item['id']].listSaleProductDiscountDto.add(product);
      }
    });
    return resultMap.values.toList();
  }

  Future<List<Menu>> getListMenuProductForManager() async {
    final dbHelper = DbHelper();
    var db = await dbHelper.instance;
    List<Map<String, dynamic>> flatResult = await db.rawQuery("""
        SELECT 
        m.id, m.name, mpp.productId,
        p.id as productId, p.productName, p.orinalPrice, p.price,
        p.quantity, p.productAvatar, p.description,
        p.productCode, p.status
        FROM
        $tableMenu m
        LEFT JOIN
        $tableMenuProductMapping mpp
        ON m.id = mpp.menuId
        LEFT JOIN 
        $tableProduct p
        ON mpp.productId = p.id
        WHERE m.id != "${VirtualMenu.ALL}" AND m.id != "${VirtualMenu.TOP_SELLING}"
    """);
    List<Menu> result = [];

    Map<String, Menu> resultMap = Map<String, Menu>();
    flatResult.forEach((item) {
      Product product = Product(
          id: item['productId'],
          productName: item['productName'],
          orinalPrice: item['orinalPrice'],
          price: item['price'],
          quantity: item['quantity'],
          productAvatar: item['productAvatar'],
          description: item['description'],
          printerId: item['printerId'],
          productCode: item['productCode'],
          status: item['status']);
      if (resultMap[item['id']] == null) {
        List<Product> productList = product.id != null ? [product] : [];
        resultMap[item['id']] = Menu(
            merchantMenu: MerchantMenu(id: item['id'], name: item['name']),
            listSaleProductDiscountDto: productList);
      } else if (product.id != null) {
        resultMap[item['id']].listSaleProductDiscountDto.add(product);
      }
    });
    return resultMap.values.toList();
  }
}

class ProductDB extends BaseDB<Product> {
  @override
  String getTableName() {
    return tableProduct;
  }

  @override
  Product fromJson(Map<String, dynamic> row) {
    return Product.fromJson(row);
  }
}
