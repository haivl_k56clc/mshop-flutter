// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    userId: json['userId'] as String,
    userName: json['userName'] as String,
    name: json['name'] as String,
    phone: json['phone'] as String,
    bankAccount: json['bankAccount'] as String,
    accessToken: json['accessToken'] as String,
    avatar: json['avatar'] as String,
    tenantCode: json['tenantCode'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'userId': instance.userId,
      'userName': instance.userName,
      'name': instance.name,
      'phone': instance.phone,
      'bankAccount': instance.bankAccount,
      'accessToken': instance.accessToken,
      'avatar': instance.avatar,
      'tenantCode': instance.tenantCode,
    };
