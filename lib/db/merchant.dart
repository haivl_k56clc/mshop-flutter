import './base_entity.dart';
import './base_dao.dart';
import 'package:json_annotation/json_annotation.dart';
part 'merchant.g.dart';

final String tableMerchant = 'merchant';
final String columnId = 'id';
final String columnShortName = 'shortName';
final String columnPhone = 'phone';
final String columnEmail = 'email';
final String columnAddress = 'address';
final String columnLa = 'la';
final String columnLo = 'lo';
final String columnWebsite = 'website';
final String columnDescription = 'description';
final String columnLogo = 'logo';
final String columnTenantCode = 'tenantCode';
final String columnTaxCode = 'taxCode';
final String columnSaleCode = 'saleCode';
final String columnWifiName = 'wifiName';
final String columnWifiPass = 'wifiPass';
final String columnCreatedAt = 'createdAt';
final String columnLastModifiedAt = 'lastModifiedAt';

final String createMerchantTableQuery = '''
        CREATE TABLE $tableMerchant ( 
          $columnId TEXT primary key, 
          $columnShortName TEXT,
          $columnPhone TEXT,
          $columnEmail TEXT,
          $columnAddress TEXT,
          $columnLa REAL,
          $columnLo REAL,
          $columnLogo TEXT,
          $columnTenantCode TEXT,
          $columnTaxCode TEXT,
          $columnSaleCode TEXT,
          $columnWifiName TEXT,
          $columnWifiPass TEXT,
          $columnCreatedAt INT,
          $columnLastModifiedAt INT
        )
''';

@JsonSerializable()
class Merchant extends BaseEntity {
  String id;
  String shortName;
  String phone;
  String email;
  String address;
  double la;
  double lo;
  String logo;
  String tenantCode;
  String taxCode;
  String saleCode;
  String wifiName;
  String wifiPass;
  int createdAt;
  int lastModifiedAt;

  Merchant(
      {this.id,
      this.shortName,
      this.phone,
      this.email,
      this.address,
      this.la,
      this.lo,
      this.logo,
      this.tenantCode,
      this.taxCode,
      this.saleCode,
      this.wifiName,
      this.wifiPass,
      this.createdAt,
      this.lastModifiedAt});

  factory Merchant.fromJson(Map<String, dynamic> json) =>
      _$MerchantFromJson(json);
  Map<String, dynamic> toJson() => _$MerchantToJson(this);

  // Merchant.fromJson(Map<String, dynamic> map) {
  //   fromJson(map);
  // }

  // void fromJson(Map<String, dynamic> map) {
  //   id = map[columnId];
  //   shortName = map[columnShortName];
  //   phone = map[columnPhone];
  //   email = map[columnEmail];
  //   address = map[columnAddress];
  //   la = map[columnLa];
  //   lo = map[columnLo];
  //   logo = map[columnLogo];
  //   tenantCode = map[columnTenantCode];
  //   taxCode = map[columnTaxCode];
  //   saleCode = map[columnSaleCode];
  //   wifiName = map[columnWifiName];
  //   wifiPass = map[columnWifiPass];
  //   createdAt = map[columnCreatedAt];
  //   lastModifiedAt = map[columnLastModifiedAt];
  // }

  // Map<String, dynamic> toJson() {
  //   var map = <String, dynamic>{
  //     columnId: id,
  //     columnShortName: shortName,
  //     columnPhone: phone,
  //     columnEmail: email,
  //     columnAddress: address,
  //     columnLa: la,
  //     columnLo: lo,
  //     columnLogo: logo,
  //     columnTenantCode: tenantCode,
  //     columnTaxCode: taxCode,
  //     columnSaleCode: saleCode,
  //     columnWifiName: wifiName,
  //     columnWifiPass: wifiPass,
  //     columnCreatedAt: createdAt,
  //     columnLastModifiedAt: lastModifiedAt,
  //   };
  //   return map;
  // }
}

class MerchantDB extends BaseDB<Merchant> {
  static Merchant _instance;
  @override
  String getTableName() {
    // TODO: implement getTableName
    return tableMerchant;
  }

  @override
  Merchant fromJson(Map<String, dynamic> row) {
    // TODO: implement fromJson
    return Merchant.fromJson(row);
  }

  static Future<Merchant> getMerchantCached() async {
    if (_instance == null) {
      MerchantDB merchantDB = MerchantDB();
      _instance = await merchantDB.getOne();
    }
    return _instance;
  }
}
