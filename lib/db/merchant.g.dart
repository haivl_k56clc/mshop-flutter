// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'merchant.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Merchant _$MerchantFromJson(Map<String, dynamic> json) {
  return Merchant(
    id: json['id'] as String,
    shortName: json['shortName'] as String,
    phone: json['phone'] as String,
    email: json['email'] as String,
    address: json['address'] as String,
    la: (json['la'] as num)?.toDouble(),
    lo: (json['lo'] as num)?.toDouble(),
    logo: json['logo'] as String,
    tenantCode: json['tenantCode'] as String,
    taxCode: json['taxCode'] as String,
    saleCode: json['saleCode'] as String,
    wifiName: json['wifiName'] as String,
    wifiPass: json['wifiPass'] as String,
    createdAt: json['createdAt'] as int,
    lastModifiedAt: json['lastModifiedAt'] as int,
  );
}

Map<String, dynamic> _$MerchantToJson(Merchant instance) => <String, dynamic>{
      'id': instance.id,
      'shortName': instance.shortName,
      'phone': instance.phone,
      'email': instance.email,
      'address': instance.address,
      'la': instance.la,
      'lo': instance.lo,
      'logo': instance.logo,
      'tenantCode': instance.tenantCode,
      'taxCode': instance.taxCode,
      'saleCode': instance.saleCode,
      'wifiName': instance.wifiName,
      'wifiPass': instance.wifiPass,
      'createdAt': instance.createdAt,
      'lastModifiedAt': instance.lastModifiedAt,
    };
