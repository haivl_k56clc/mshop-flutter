import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:new_project/api/common.dart';

class HomeProvider with ChangeNotifier {
  bool isLoading;
  int dailyRevenue = 0;
  int waitingOrder = 0;
  int paidOrder = 0;

  Future<void> getHomeStatistic() async {
    Response staticalRes = await get('/home/statistical');
    if (staticalRes.statusCode == 200) {
      Map<String, dynamic> result = staticalRes.data;
      dailyRevenue = result["totalTransactionAmount"] as int;
      waitingOrder = result["numTransactionPending"] as int;
      paidOrder = result["numTransactionCompleted"] as int;
      notifyListeners();
    }
  }
}
