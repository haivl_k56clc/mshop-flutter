import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'dart:developer' as developer;

String getSha256(String data) {
  var bytes = utf8.encode(data);
  var digest = sha256.convert(bytes);
  return digest.toString();
}

dynamic chainParse(Map<String, dynamic> data, List<String> keys) {
  try {
    dynamic nestedObj = data;
    int keysLength = keys.length;
    for (int i = 0; i < keysLength; i++) {
      var currentKey = keys[i];
      nestedObj = nestedObj[currentKey];
      if (nestedObj == null) return null;
    }
    return nestedObj;
  } catch (e) {
    developer.log("Chainparse err: " + e.toString());
    return null;
  }
}
