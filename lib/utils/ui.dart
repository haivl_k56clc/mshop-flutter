import 'package:flutter/material.dart';
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/themes/colors.dart';

showLoadingDialog(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        contentPadding: EdgeInsets.all(24),
        elevation: 2,
        backgroundColor: AppColor.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        content: Container(
          child: Row(
            children: <Widget>[
              CircularProgressIndicator(),
              SizedBox(width: 16),
              Text('Loading...', style: defaultTextStyle)
            ],
          ),
        ),
      );
    },
  );
}

hideLoadingDialog(BuildContext context) {
  if (Navigator.of(context).canPop()) {
    Navigator.of(context).pop();
  }
}

showSnackBar(BuildContext context, String text) {
  final scaffold = Scaffold.of(context);
  scaffold.showSnackBar(
    SnackBar(
      content: Text(text),
    ),
  );
}
