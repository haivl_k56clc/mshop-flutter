import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/widgets/toolbar.dart';
import 'package:new_project/bloc/product_bloc.dart';
import 'package:new_project/db/product.dart';
import 'dart:developer' as developer;
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/widgets/product_item.dart';

class ProductSelector extends StatefulWidget {
  static const String screenName = '/product_selector';
  @override
  _ProductSelectorState createState() => _ProductSelectorState();
}

class _ProductSelectorState extends State<ProductSelector>
    with TickerProviderStateMixin {
  final bloc = ProductBloc();
  TabController _tabController;
  List<Menu> menuData = [];
  @override
  void initState() {
    super.initState();
    bloc.loadProduct();
    _tabController = TabController(vsync: this, length: 0);
  }

  @override
  void dispose() {
    bloc.dispose();
    _tabController.dispose();
    super.dispose();
  }

  Widget _buildListItem(BuildContext context, int index, int tabIndex) {
    Product productData = menuData[tabIndex].listSaleProductDiscountDto[index];
    return ProductItem(product: productData);
  }

  List<Widget> _getTabs() {
    return menuData
        .map((item) => Text(
              item?.merchantMenu?.name,
              style: TextStyle(fontFamily: 'SFProText-Medium'),
            ))
        .toList();
  }

  List<Widget> _getLists() {
    List<Widget> lists = [];
    for (int i = 0; i < menuData.length; i++) {
      lists.add(
        ListView.builder(
          itemBuilder: (BuildContext context, int index) =>
              _buildListItem(context, index, i),
          itemCount: menuData[i].listSaleProductDiscountDto.length,
        ),
      );
    }
    return lists;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: AppColor.white,
          body: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Toolbar(title: 'CHỌN SẢN PHẨM'),
                  StreamBuilder<List<Menu>>(
                    initialData: [],
                    stream: bloc.listMenuStream,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        if (menuData.length != snapshot.data.length) {
                          _tabController = TabController(
                              vsync: this, length: snapshot.data.length);
                        }
                        menuData = snapshot.data;

                        return Expanded(
                          child: Column(
                            children: <Widget>[
                              TabBar(
                                isScrollable: true,
                                tabs: _getTabs(),
                                labelColor: AppColor.primary,
                                labelPadding: EdgeInsets.symmetric(
                                  vertical: 16,
                                  horizontal: 24,
                                ),
                                indicatorSize: TabBarIndicatorSize.label,
                                indicatorWeight: 3,
                                controller: _tabController,
                              ),
                              Expanded(
                                child: TabBarView(
                                  controller: _tabController,
                                  children: _getLists(),
                                ),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return CircularProgressIndicator();
                      }
                    },
                  ),
                ],
              ),
              // Positioned(
              //   bottom: 0,
              //   left: 0,
              //   right: 0,
              //   child: Row(
              //     children: <Widget>[
              //       Expanded(
              //         child: SizedBox(
              //           height: 48,
              //           child: FlatButton(
              //             color: AppColor.babyBlue,
              //             splashColor: AppColor.white,
              //             shape: RoundedRectangleBorder(
              //               borderRadius: BorderRadius.circular(0),
              //             ),
              //             child: Text(
              //               "Thêm danh mục",
              //               style: TextStyle(
              //                 color: AppColor.primary,
              //                 fontSize: 14,
              //                 fontWeight: FontWeight.bold,
              //                 letterSpacing: -0.24,
              //               ),
              //             ),
              //             onPressed: () {},
              //           ),
              //         ),
              //       ),
              //       Expanded(
              //         child: SizedBox(
              //           height: 48,
              //           child: FlatButton(
              //             color: AppColor.cerulean,
              //             splashColor: AppColor.white,
              //             shape: RoundedRectangleBorder(
              //               borderRadius: BorderRadius.circular(0),
              //             ),
              //             child: Text(
              //               "Thêm sản phẩm",
              //               style: TextStyle(
              //                 color: AppColor.white,
              //                 fontSize: 14,
              //                 fontWeight: FontWeight.bold,
              //                 letterSpacing: -0.24,
              //               ),
              //             ),
              //             onPressed: () {},
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
