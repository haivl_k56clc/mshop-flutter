import 'dart:async';
import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/localization.dart';
import 'package:new_project/db/user.dart';
import 'package:new_project/db/merchant.dart';

class Splash extends StatefulWidget {
  const Splash({Key key}) : super(key: key);
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  // final UserDatabase userDatabase = UserDatabase();
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 1), () async {
      User user = await UserDB.getUserCached();
      Merchant merchant = await MerchantDB.getMerchantCached();
      String startRoute = user == null ? '/login' : '/home';
      Navigator.pushReplacementNamed(context, startRoute);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.cerulean,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: double.infinity, height: 159),
          Image.asset('images/logo.png',
              width: 110, height: 120, fit: BoxFit.cover),
          SizedBox(width: double.infinity, height: 31),
          Text(
            AppLocalizations.of(context).t('sologan'),
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: AppColor.babyBlue,
              letterSpacing: -0.27,
              fontSize: 18,
              height: 1.33333,
            ),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
