import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
// import 'package:new_project/db/user.dart';
// import 'package:new_project/providers/user_provider.dart';
// import 'package:provider/provider.dart';
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/widgets/toolbar.dart';
import 'package:new_project/widgets/row_text_input.dart';
import 'dart:async';

class AccountInfo extends StatefulWidget {
  static const String screenName = '/account_info';
  @override
  _AccountInfoState createState() => _AccountInfoState();
}

class _AccountInfoState extends State<AccountInfo> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  // UserData userData;
  String _phone;
  String _name;

  @override
  void initState() {
    super.initState();
    _nameController.addListener(() {
      setState(() {
        _name = _nameController.text;
      });
    });
    _phoneController.addListener(() {
      setState(() {
        _phone = _phoneController.text;
      });
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _phoneController.dispose();
    super.dispose();
  }

  _handleSave() {}

  // Future<UserData> _fetchUserData() {
  //   return Future.delayed(Duration(milliseconds: 0), () {
  //     userData = Provider.of<UserProvider>(context).userData;
  //     if (_nameController != null && _nameController.text.isEmpty) {
  //       _nameController.text = userData.name;
  //     }
  //     if (_nameController != null && _phoneController.text.isEmpty) {
  //       _phoneController.text = userData.phone;
  //     }
  //     return userData;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    bool enableBtn = true;
    //  (_name != null &&
    //     _name.isNotEmpty &&
    //     _phone != null &&
    //     _phone.isNotEmpty);

    return Container(
      color: AppColor.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: AppColor.backgroundColor,
          body: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Toolbar(title: 'THÔNG TIN TÀI KHOẢN'),
                  Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(24, 16, 16, 16),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text('Tên đăng nhập',
                                      style: captionTextBlackStyle),
                                  SizedBox(height: 5),
                                  Text("Hai Vu", style: defaultBoldTextStyle),
                                ],
                              ),
                            ),
                            InkWell(
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color: AppColor.white,
                                  borderRadius: BorderRadius.circular(6),
                                  border: Border.all(
                                      color: AppColor.borderColor,
                                      width: 1,
                                      style: BorderStyle.solid),
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Image.asset('images/lock.png',
                                        width: 24,
                                        height: 24,
                                        fit: BoxFit.cover),
                                    SizedBox(width: 8),
                                    Text(
                                      'Đổi mật khẩu',
                                      style: captionTextBlackStyle,
                                    ),
                                  ],
                                ),
                              ),
                              onTap: () {},
                            ),
                          ],
                        ),
                      ),
                      RowTextInput(
                        label: 'Họ và tên',
                        controller: _nameController,
                      ),
                      SizedBox(height: 10),
                      RowTextInput(
                        label: 'Số điện thoại',
                        controller: _phoneController,
                      ),
                    ],
                  )
                  // FutureBuilder(
                  //   future: _fetchUserData(),
                  //   builder: (BuildContext context, AsyncSnapshot snapshot) {
                  //     if (snapshot.hasData) {
                  //       return
                  //     } else {
                  //       return SizedBox(width: 0, height: 0);
                  //     }
                  //   },
                  // ),
                ],
              ),
              Positioned(
                child: SizedBox(
                  child: RaisedButton(
                    color: AppColor.cerulean,
                    child: Text('Lưu thay đổi', style: buttonActiveTextStyle),
                    onPressed: enableBtn ? _handleSave : null,
                    splashColor: AppColor.white,
                  ),
                  height: 46,
                ),
                bottom: 0,
                left: 0,
                right: 0,
              )
            ],
          ),
        ),
      ),
    );
  }
}
