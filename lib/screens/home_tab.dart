import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/localization.dart';
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/bloc/home_bloc.dart';
import 'package:new_project/widgets/touchable.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> with AutomaticKeepAliveClientMixin {
  final bloc = HomeBloc();
  void initState() {
    super.initState();
    bloc.loadStatistic();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  _handleCreateOrder() {
    print('_handleCreateOrder');
  }

  _handleCreateExpense() {
    print('_handleCreateExpense');
  }

  String _getDisplayStatistic(int value) {
    if (value == null || value == 0) return '--';
    return value.toString();
  }

  Widget _buildHeader() {
    return StreamBuilder<Map<String, int>>(
      stream: bloc.statisticStream,
      initialData: {},
      builder: (context, snapshot) {
        Map<String, int> statisticData = snapshot?.data;
        return SizedBox(
          width: double.infinity,
          height: 236,
          child: Column(
            children: <Widget>[
              SizedBox(
                width: double.infinity,
                height: 40,
              ),
              Image.asset(
                'images/logo_login.png',
                width: 29,
                height: 32,
                fit: BoxFit.contain,
              ),
              SizedBox(
                width: double.infinity,
                height: 30,
              ),
              Text(
                AppLocalizations.of(context).t('day_revenue'),
                style: dashboardLabelTextStyle,
                textAlign: TextAlign.center,
              ),
              Text(
                _getDisplayStatistic(statisticData['totalTransactionAmount']),
                style: dashboardInfoTextStyle,
                textAlign: TextAlign.center,
              ),
              SizedBox(
                width: double.infinity,
                height: 24,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context).t('waiting_order'),
                          style: dashboardLabelTextStyle,
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          _getDisplayStatistic(
                              statisticData['numTransactionPending']),
                          style: dashboardInfoTextStyle,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Text(
                          AppLocalizations.of(context).t('paid_order'),
                          style: dashboardLabelTextStyle,
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          _getDisplayStatistic(
                              statisticData['numTransactionCompleted']),
                          style: dashboardInfoTextStyle,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildActionButton() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 24),
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6), color: AppColor.white),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Touchable(
              onTap: _handleCreateOrder,
              child: Container(
                padding: const EdgeInsets.fromLTRB(8, 16, 6, 16),
                decoration: BoxDecoration(
                  color: AppColor.babyBlue10,
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      'images/create_order_home.png',
                      width: 32,
                      height: 32,
                      fit: BoxFit.contain,
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        AppLocalizations.of(context).t('create_order'),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: AppColor.cerulean,
                          fontSize: 12,
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    Image.asset(
                      'images/chevron_right.png',
                      width: 6,
                      height: 10,
                      fit: BoxFit.contain,
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 20, width: 5),
          Expanded(
            child: Touchable(
              child: Container(
                padding: const EdgeInsets.fromLTRB(8, 16, 6, 16),
                decoration: BoxDecoration(
                  color: AppColor.babyBlue10,
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      'images/create_expense_home.png',
                      width: 32,
                      height: 32,
                      fit: BoxFit.contain,
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        AppLocalizations.of(context).t('create_expense'),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: AppColor.cerulean,
                          fontSize: 12,
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    Image.asset(
                      'images/chevron_right.png',
                      width: 6,
                      height: 10,
                      fit: BoxFit.contain,
                    ),
                  ],
                ),
              ),
              onTap: _handleCreateExpense,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor.cerulean,
      child: SafeArea(
        child: Container(
          color: AppColor.backgroundColor,
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 302,
                    decoration: BoxDecoration(
                      color: AppColor.cerulean,
                      borderRadius: BorderRadius.vertical(
                        top: Radius.zero,
                        bottom: Radius.circular(6.0),
                      ),
                    ),
                  )
                ],
              ),
              Column(
                children: <Widget>[
                  _buildHeader(),
                  SizedBox(width: double.infinity, height: 16),
                  _buildActionButton()
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
