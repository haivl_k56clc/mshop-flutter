import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/widgets/text_input_left_icon.dart';
import 'package:new_project/widgets/rounded_button.dart';
import 'package:new_project/localization.dart';
import 'package:new_project/bloc/login_bloc.dart';
import 'package:new_project/utils/ui.dart';

class Login extends StatefulWidget {
  static const String screenName = '/login';
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String _tenantCode;
  String _userName;
  String _password;
  FocusNode _tenantCodeFocus;
  FocusNode _userNameFocus;
  FocusNode _passwordFocus;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final bloc = LoginBloc();

  void initState() {
    super.initState();
    _tenantCodeFocus = FocusNode();
    _userNameFocus = FocusNode();
    _passwordFocus = FocusNode();
    bloc.loginStream.listen(handleLoginEvent);
  }

  void dispose() {
    _tenantCodeFocus.dispose();
    _userNameFocus.dispose();
    _passwordFocus.dispose();
    bloc.dispose();
    super.dispose();
  }

  void handleLoginEvent(LoginEvent event) {
    var eventType = event.type;
    var eventMessage = event.message;
    if (eventType == LoginEventType.startLogin) {
      showLoadingDialog(context);
      return;
    } else if (eventType == LoginEventType.loginSuccess) {
      hideLoadingDialog(context);
      Navigator.of(context).pushReplacementNamed('/home');
      return;
    } else if (eventType == LoginEventType.loginFail) {
      hideLoadingDialog(context);
      SnackBar snackBar = SnackBar(content: Text(eventMessage));
      _scaffoldKey?.currentState?.showSnackBar(snackBar);
      return;
    }
  }

  _handleForgotPassword() {
    print('_handleForgotPassword');
  }

  _handleLogin() {
    bloc.login();
  }

  _handleRegister() {
    print('_handleRegister');
  }

  _jumpFocus(BuildContext context, FocusNode currentNode, FocusNode nextNode) {
    currentNode.unfocus();
    FocusScope.of(context).requestFocus(nextNode);
  }

  @override
  Widget build(BuildContext context) {
    bool enableBtn = true;
    // (_tenantCode != null && _userName != null && _password != null);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AppColor.backgroundColor,
      body: Stack(
        children: <Widget>[
          Container(
            height: 334,
            decoration: BoxDecoration(
              color: AppColor.cerulean,
              borderRadius: BorderRadius.vertical(
                top: Radius.zero,
                bottom: Radius.circular(6.0),
              ),
            ),
          ),
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(width: double.infinity, height: 40),
                Image.asset(
                  'images/logo_login.png',
                  width: 64,
                  height: 70,
                  fit: BoxFit.contain,
                ),
                Text(
                  AppLocalizations.of(context).t('login'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: AppColor.babyBlue,
                    letterSpacing: -0.27,
                    fontSize: 18,
                    height: 1.33333,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(width: double.infinity, height: 16),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 24.0),
                  decoration: BoxDecoration(
                    color: AppColor.white,
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 32.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(width: double.infinity, height: 32),
                        TextInput(
                          leftIcon: 'home_login',
                          placeholder:
                              AppLocalizations.of(context).t('store_name'),
                          onChanged: (text) {
                            setState(() {
                              _tenantCode = text.trim();
                            });
                          },
                          textInputAction: TextInputAction.next,
                          onSubmitted: (String value) => _jumpFocus(
                              context, _tenantCodeFocus, _userNameFocus),
                          focusNode: _tenantCodeFocus,
                        ),
                        SizedBox(width: double.infinity, height: 16),
                        TextInput(
                          leftIcon: 'avatar_login',
                          placeholder:
                              AppLocalizations.of(context).t('user_name'),
                          onChanged: (text) {
                            setState(() {
                              _userName = text.trim();
                            });
                          },
                          textInputAction: TextInputAction.next,
                          focusNode: _userNameFocus,
                          onSubmitted: (String value) => _jumpFocus(
                              context, _userNameFocus, _passwordFocus),
                        ),
                        SizedBox(width: double.infinity, height: 16),
                        TextInput(
                            leftIcon: 'password_login',
                            placeholder:
                                AppLocalizations.of(context).t('password'),
                            obscureText: true,
                            onChanged: (text) {
                              setState(() {
                                _password = text.trim();
                              });
                            },
                            textInputAction: TextInputAction.done,
                            focusNode: _passwordFocus,
                            onSubmitted: (String value) => _handleLogin()),
                        SizedBox(width: double.infinity, height: 32),
                        RoundedButton(
                          text: AppLocalizations.of(context).t('login'),
                          onPressed: _handleLogin,
                          disabled: !enableBtn,
                        ),
                        SizedBox(width: double.infinity, height: 24),
                        InkWell(
                          child: Text(
                            AppLocalizations.of(context).t('forgot_password'),
                            style: TextStyle(
                              color: AppColor.textBlack,
                              fontSize: 14,
                              letterSpacing: -0.24,
                            ),
                          ),
                          onTap: _handleForgotPassword,
                        ),
                        SizedBox(width: double.infinity, height: 36),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: double.infinity, height: 24),
                Text(
                  AppLocalizations.of(context).t('not_have_account'),
                  style: TextStyle(
                    color: AppColor.textBlack,
                    fontSize: 14,
                    letterSpacing: -0.24,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 56),
                  child: Column(
                    children: <Widget>[
                      SizedBox(width: double.infinity, height: 12),
                      SizedBox(
                        width: double.infinity,
                        height: 40,
                        child: FlatButton(
                          color: AppColor.black5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6.0),
                          ),
                          child: Text(
                            AppLocalizations.of(context)
                                .t('create_free_account'),
                            style: TextStyle(
                              color: AppColor.cerulean,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              letterSpacing: -0.24,
                            ),
                          ),
                          onPressed: _handleRegister,
                        ),
                      ),
                      SizedBox(width: double.infinity, height: 32),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
