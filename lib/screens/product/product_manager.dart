import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/widgets/toolbar.dart';
import 'package:new_project/bloc/product_bloc.dart';
import 'package:new_project/db/product.dart';
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/widgets/product_item.dart';
import 'package:new_project/screens/product/product_info.dart';

class ProductManager extends StatefulWidget {
  static const String screenName = 'product_manager';

  @override
  _ProductManagerState createState() => _ProductManagerState();
}

class _ProductManagerState extends State<ProductManager> {
  final bloc = ProductBloc();
  List<Menu> menuData = [];
  @override
  void initState() {
    super.initState();
    // _load();
    bloc.loadProductForManager();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  _handlePressProductItem(Product product) {
    print('ProductId: ' + product.id);
    Navigator.of(context).pushNamed(ProductInfo.screenName, arguments: product);
  }

  Widget _buildListItem(BuildContext context, int index) {
    return ExpansionTile(
      title: Container(
        child: Text(
          menuData[index].merchantMenu.name,
          style: defaultTextMediumStyle,
        ),
      ),
      children: menuData[index].listSaleProductDiscountDto.map(
        (product) {
          return ProductItem(
            product: product,
            onPress: _handlePressProductItem,
          );
        },
      ).toList(),
    );
    // ListTile(title: Text(menuData[index].merchantMenu.name));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: AppColor.white,
          body: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Toolbar(title: 'QUẢN LÝ SẢN PHẨM'),
                  Expanded(
                    child: StreamBuilder<List<Menu>>(
                      initialData: [],
                      stream: bloc.listMenuStream,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          menuData = snapshot.data;
                          return ListView.builder(
                            itemBuilder: _buildListItem,
                            itemCount: snapshot.data.length,
                          );
                        } else {
                          return CircularProgressIndicator();
                        }
                      },
                    ),
                  ),
                ],
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: SizedBox(
                        height: 48,
                        child: FlatButton(
                          color: AppColor.babyBlue,
                          splashColor: AppColor.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                          ),
                          child: Text(
                            "Thêm danh mục",
                            style: TextStyle(
                              color: AppColor.primary,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              letterSpacing: -0.24,
                            ),
                          ),
                          onPressed: () {},
                        ),
                      ),
                    ),
                    Expanded(
                      child: SizedBox(
                        height: 48,
                        child: FlatButton(
                          color: AppColor.cerulean,
                          splashColor: AppColor.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                          ),
                          child: Text(
                            "Thêm sản phẩm",
                            style: TextStyle(
                              color: AppColor.white,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              letterSpacing: -0.24,
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context)
                                .pushNamed(ProductInfo.screenName);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
