import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/utils/ui.dart';
import 'package:new_project/widgets/toolbar.dart';
import 'package:new_project/db/product.dart';
import 'package:new_project/widgets/rounded_checkbox.dart';
import 'package:new_project/widgets/form_text_input.dart';
import 'package:new_project/widgets/form_image_picker.dart';
import 'package:new_project/widgets/multiple_tag_selector.dart';
import 'dart:io';
import 'package:new_project/api/product.dart';

class ProductInfo extends StatefulWidget {
  static const String screenName = '/product_info';
  @override
  _ProductInfoState createState() => _ProductInfoState();
}

class _ProductInfoState extends State<ProductInfo> {
  // final bloc = ProductBloc();
  // List<Menu> menuData = [];
  String _status = '1';
  File _localImage = null;
  String _name;
  String _price;
  String _description;
  List<String> _categories = [];

  @override
  void initState() {
    super.initState();
    // _load();
    // bloc.loadProductForManager();
  }

  @override
  void dispose() {
    // bloc.dispose();
    super.dispose();
  }

  _handleChangeSaleStatus(CheckboxData checkboxData) {
    setState(() {
      _status = checkboxData.id;
    });
  }

  Widget _buildStatusCheckbox() {
    return Container(
      height: 48,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: AppColor.borderColor2,
          ),
        ),
        color: AppColor.white,
      ),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 118,
            height: 48,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(24, 16, 16, 16),
              child: Text(
                'Trạng thái',
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(color: AppColor.textBlack),
              ),
            ),
          ),
          Container(
            width: 1,
            height: 48,
            color: AppColor.borderColor2,
          ),
          HorizontalCheckboxList(
            data: [
              CheckboxData(id: '1', title: 'Đang bán'),
              CheckboxData(id: '2', title: 'Đã bán')
            ],
            value: _status,
            onChange: _handleChangeSaleStatus,
          )
        ],
      ),
    );
  }

  _handleChangeAvatarImageFile(File imageFile) {
    setState(() {
      _localImage = imageFile;
    });
  }

  _handleDeleteAvatarImage() {
    setState(() {
      _localImage = null;
    });
  }

  _handleChangeCategory(List<String> newCategories) {
    setState(() {
      _categories = newCategories;
    });
  }

  _handleChangeName(String text) {
    setState(() {
      _name = text;
    });
  }

  _handleChangePrice(String text) {
    setState(() {
      _price = text;
    });
  }

  _handleChangeDescription(String text) {
    setState(() {
      _description = text;
    });
  }

  bool _isEnableSave() {
    return _name != null && _price != null;
  }

  _handleSave() async {
    print('_handleSave');
    Map<String, dynamic> requestObj = {
      "name": _name.trim(),
      // "productCode": this.state.productCode.trim(),
      "description": _description?.trim() ?? '',
      "originalPrice": int.parse(_price),
      "price": int.parse(_price),
      "quantity": '', // +revertFormatMoney(this.state.quantity)
      "category": 15, // Ăn uống //this.state.categoryId,
      "avatarImage": '',
      "listImage": '',
      "updateVariant": true,
      "updateImage": true,
      "searchable": 1,
      "menuId": '',
      "status": 0,
      "printerId": null
    };
    showLoadingDialog(context);
    final createProductRes = await createProduct(requestObj);
    print('createProductRes: ' + createProductRes.headers.toString());
    hideLoadingDialog(context);
    if (createProductRes.statusCode == 200) {
      Fluttertoast.showToast(
        msg: 'Thêm sản phẩm thành công',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIos: 1,
        backgroundColor: AppColor.primary,
        textColor: AppColor.white,
        fontSize: 14.0,
      );
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    // final Product product = ModalRoute.of(context).settings.arguments;
    bool enableBtn = _isEnableSave();
    return Container(
      color: AppColor.white,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomPadding: true,
          backgroundColor: AppColor.backgroundColor,
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Toolbar(title: 'THÔNG TIN SẢN PHẨM'),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            _buildStatusCheckbox(),
                            Container(height: 12),
                            FormImagePicker(
                              label: 'Ảnh sản phẩm',
                              localFile: _localImage,
                              onChangeFile: _handleChangeAvatarImageFile,
                              onDeleteImage: _handleDeleteAvatarImage,
                            ),
                            Container(height: 12),
                            FormTextInput(
                              label: 'Tên sản phẩm',
                              onChanged: _handleChangeName,
                            ),
                            FormTextInput(
                              label: 'Giá bán',
                              keyboardType: TextInputType.number,
                              onChanged: _handleChangePrice,
                            ),
                            // FormTextInput(
                            //   label: 'Số lượng',
                            //   keyboardType: TextInputType.number,
                            // ),
                            SizedBox(height: 12),
                            MultipleTagSelector(
                              label: 'Chọn danh mục',
                              data: [
                                TagData(id: '1', label: 'Đồ ăn chính'),
                                TagData(id: '2', label: 'Đồ ăn vặt'),
                                TagData(id: '3', label: 'Chè'),
                                TagData(id: '4', label: 'Kem'),
                              ],
                              values: _categories,
                              onChange: _handleChangeCategory,
                            ),
                            SizedBox(height: 12),
                            FormTextInput(
                                label: 'Mô tả',
                                multiline: true,
                                keyboardType: TextInputType.multiline,
                                onChanged: _handleChangeDescription),
                            SizedBox(height: 60),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: SizedBox(
                    height: 48,
                    child: FlatButton(
                      color: AppColor.cerulean,
                      splashColor: AppColor.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0),
                      ),
                      child: Text(
                        "Lưu thông tin",
                        style: TextStyle(
                          color: AppColor.white,
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          letterSpacing: -0.24,
                        ),
                      ),
                      onPressed: enableBtn ? _handleSave : null,
                      disabledColor: Colors.grey,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
