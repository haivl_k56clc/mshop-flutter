import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
// import 'package:new_project/themes/theme.dart';
import 'package:new_project/screens/product_selector.dart';

class OrderTab extends StatefulWidget {
  @override
  _OrderTabState createState() => _OrderTabState();
}

class _OrderTabState extends State<OrderTab> {
  _handleCreateOrder() {
    Navigator.of(context).pushNamed(ProductSelector.screenName);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        // The number of tabs / content sections to display.
        length: 2,
        child: Scaffold(
          appBar: TabBar(
            tabs: [
              Text(
                'ĐƠN ĐANG CHỜ',
                style: TextStyle(fontFamily: 'SFProText-Medium'),
              ),
              Text(
                'ĐÃ XỬ LÝ',
                style: TextStyle(fontFamily: 'SFProText-Medium'),
              ),
            ],
            labelColor: AppColor.primary,
            labelPadding: EdgeInsets.symmetric(vertical: 16),
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 3,
          ),
          body: Stack(
            children: <Widget>[
              TabBarView(
                children: [Text('Tab 1'), Text('Tab 2')],
              ),
              Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        color: AppColor.cerulean,
                        splashColor: AppColor.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Text(
                          "Tạo đơn",
                          style: TextStyle(
                            color: AppColor.white,
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            letterSpacing: -0.24,
                          ),
                        ),
                        onPressed: _handleCreateOrder,
                      ),
                    ],
                  ))
            ],
          ),
        ),
      ), // Complete this code in the next step.
    );
  }
}
