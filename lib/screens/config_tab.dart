import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/widgets/rounded_button.dart';
import 'package:new_project/localization.dart';
import 'package:new_project/themes/text_styles.dart';
import 'package:new_project/db/user.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:new_project/screens/account_info.dart';
import 'package:new_project/screens/product/product_manager.dart';
import 'package:new_project/screens/login.dart';
import 'package:new_project/widgets/touchable.dart';

enum ItemType { header, item, info, logout }

class ConfigTab extends StatefulWidget {
  @override
  _ConfigTabState createState() => _ConfigTabState();
}

class _ConfigTabState extends State<ConfigTab> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  // final UserDatabase userDatabase = UserDatabase();
  final List configs = [
    ListItemData(type: ItemType.info),
    ListItemData(text: 'THÔNG TIN', type: ItemType.header, id: 1),
    ListItemData(text: 'Thông tin cửa hàng', type: ItemType.item, id: 2),
    ListItemData(
        text: 'Thông tin tài khoản', type: ItemType.item, id: 3, isLast: true),
    ListItemData(text: 'THIẾT LẬP CỬA HÀNG', type: ItemType.header, id: 4),
    ListItemData(text: 'Quản lý sản phẩm', type: ItemType.item, id: 5),
    ListItemData(text: 'Quản lý khu vực - bàn', type: ItemType.item, id: 6),
    ListItemData(
        text: 'Quản lý nhân viên', type: ItemType.item, id: 7, isLast: true),
    ListItemData(text: 'THÊM', type: ItemType.header, id: 8),
    ListItemData(text: 'Thiết lập QR Pay', type: ItemType.item, id: 9),
    ListItemData(text: 'Thiết lập máy in', type: ItemType.item, id: 10),
    ListItemData(text: 'Nhà cung cấp', type: ItemType.item, id: 11),
    ListItemData(text: 'Khuyến mại', type: ItemType.item, id: 12, isLast: true),
    ListItemData(type: ItemType.logout),
  ];

  _handlePressItem(ListItemData item) {
    switch (item.id) {
      case 3:
        Navigator.of(context).pushNamed(AccountInfo.screenName);
        break;
      case 5:
        Navigator.of(context).pushNamed(ProductManager.screenName);
        break;
      default:
        {
          Fluttertoast.showToast(
            msg: item.text,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.TOP,
            timeInSecForIos: 1,
            backgroundColor: AppColor.white85,
            textColor: AppColor.black85,
            fontSize: 14.0,
          );
        }
    }
  }

  _logout() async {
    await UserDB().deleteAll();
    Navigator.of(context).pushNamedAndRemoveUntil(
        Login.screenName, (Route<dynamic> route) => false);
  }

  _handleLogout() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          actions: <Widget>[
            FlatButton(
              child:
                  Text(AppLocalizations.of(context).t('cancel').toUpperCase()),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child:
                  Text(AppLocalizations.of(context).t('ok_vi').toUpperCase()),
              onPressed: _logout,
            ),
          ],
          content: Text(AppLocalizations.of(context).t('logout_confirm')),
          title: Text(AppLocalizations.of(context).t('confirm')),
        );
      },
    );
  }

  Widget _buildListItem(context, index) {
    final item = configs[index] as ListItemData;
    if (item.type == ItemType.info) {
      return _buildHeader();
    } else if (item.type == ItemType.header) {
      return Container(
        key: Key(item.id.toString()),
        padding: EdgeInsets.fromLTRB(24, 16, 24, 8),
        color: AppColor.backgroundColor,
        child: Text(
          item.text,
          style: captionTextStyle,
        ),
      );
    } else if (item.type == ItemType.logout) {
      return Container(
        key: Key(item.id.toString()),
        padding: EdgeInsets.fromLTRB(32, 16, 32, 16),
        color: AppColor.backgroundColor,
        child: Column(
          children: <Widget>[
            RoundedButton(
              text: AppLocalizations.of(context).t('logout'),
              onPressed: _handleLogout,
            ),
            SizedBox(height: 24),
            Text(
              'version 1.0.1',
              style: TextStyle(
                fontSize: 14,
                color: AppColor.black50,
                letterSpacing: -0.24,
              ),
            )
          ],
        ),
      );
    }
    return Touchable(
      onTap: () {
        _handlePressItem(item);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 24),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: item.isLast != true ? 1 : 0,
              color: AppColor.borderColor2,
            ),
          ),
        ),
        child: Row(children: <Widget>[
          Expanded(
            child: Text(
              item.text,
              style: TextStyle(
                  fontSize: 14, color: AppColor.black, height: 1.42857),
            ),
          ),
          Image.asset(
            'images/chevron_right.png',
            width: 6,
            height: 10,
            fit: BoxFit.contain,
          ),
        ]),
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      key: Key("header"),
      height: 144,
      width: double.infinity,
      color: AppColor.cerulean,
      child: Column(
        children: <Widget>[
          SizedBox(width: double.infinity, height: 32),
          Stack(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 16),
                    child: SizedBox(
                      width: 46,
                      height: 46,
                      child: FlatButton(
                        child: Image.asset(
                          'images/notification.png',
                          width: 46,
                          height: 46,
                          fit: BoxFit.contain,
                        ),
                        onPressed: () {
                          print('Press Noti');
                        },
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'images/avatar.png',
                    width: 56,
                    height: 56,
                    fit: BoxFit.cover,
                  ),
                ],
              )
            ],
          ),
          SizedBox(width: double.infinity, height: 14),
          Text(
            'VŨ LONG HẢI',
            style: TextStyle(
              color: AppColor.babyBlue,
              fontSize: 12,
              letterSpacing: -0.18,
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor.cerulean,
      child: SafeArea(
        child: Container(
          color: AppColor.backgroundColor,
          child: ListView.builder(
            itemCount: configs.length,
            itemBuilder: _buildListItem,
            padding: EdgeInsets.all(0),
          ),
        ),
      ),
    );
  }
}

class ListItemData {
  final String text;
  final ItemType type;
  final int id;
  final bool isLast;
  ListItemData({this.id, this.text, this.type, this.isLast});
}
