import 'package:flutter/material.dart';
import 'package:new_project/themes/colors.dart';
import 'package:new_project/screens/home_tab.dart';
import 'package:new_project/screens/config_tab.dart';
import 'package:new_project/screens/order_tab.dart';
import 'package:new_project/screens/accountant_tab.dart';
import 'package:new_project/localization.dart';
import 'package:new_project/themes/images.dart';

class Home extends StatefulWidget {
  static const String screenName = '/home';
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
    _pageController.addListener(() {
      setState(() {
        _selectedIndex = _pageController.page.round();
      });
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    _pageController.jumpToPage(index);
  }

  Widget _build() {
    return (PageView(
      children: <Widget>[
        HomeTab(),
        OrderTab(),
        AccountantTab(),
        ConfigTab(),
      ],
      controller: _pageController,
    ));
  }

  @override
  Widget build(BuildContext context) {
    final homeTab = (_selectedIndex == 0)
        ? AppImageAssets.bottomHomeActive
        : AppImageAssets.bottomHome;
    final orderTab = (_selectedIndex == 1)
        ? AppImageAssets.bottomOrderActive
        : AppImageAssets.bottomOrder;
    final reportTab = (_selectedIndex == 2)
        ? AppImageAssets.bottomReportActive
        : AppImageAssets.bottomReport;
    final configTab = (_selectedIndex == 3)
        ? AppImageAssets.bottomConfigActive
        : AppImageAssets.bottomConfig;

    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      body: _build(),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 6.0, top: 6.0),
              child: Image.asset(
                homeTab,
                width: 21,
                height: 21,
                fit: BoxFit.cover,
              ),
            ),
            title: Text(AppLocalizations.of(context).t('home')),
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 6.0, top: 6.0),
              child: Image.asset(
                orderTab,
                width: 21,
                height: 21,
                fit: BoxFit.cover,
              ),
            ),
            title: Text(AppLocalizations.of(context).t('order')),
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 6.0, top: 6.0),
              child: Image.asset(
                reportTab,
                width: 21,
                height: 21,
                fit: BoxFit.cover,
              ),
            ),
            title: Text(AppLocalizations.of(context).t('accountant')),
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: const EdgeInsets.only(bottom: 6.0, top: 6.0),
              child: Image.asset(
                configTab,
                width: 21,
                height: 21,
                fit: BoxFit.cover,
              ),
            ),
            title: Text(AppLocalizations.of(context).t('config')),
          ),
        ],
        showSelectedLabels: true,
        backgroundColor: AppColor.white,
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        selectedItemColor: AppColor.cerulean,
        onTap: _onItemTapped,
        iconSize: 30,
      ),
    );
  }
}
