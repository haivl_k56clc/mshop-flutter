import 'common.dart';
import 'package:dio/dio.dart';
import 'dart:async';

Future<Response> getMenu() {
  return get('/v2/merchant/get-merchant-menu-product');
}

Future<Response> getProduct() {
  return get('/v2/product/get-product-list', {"page": 0});
}

Future<Response> createProduct(Map<String, dynamic> requestObj) {
  return post('/product/create-product', requestObj);
}

Future<Response> updateProduct(Map<String, dynamic> requestObj) {
  return post('/product/update-product', requestObj);
}
