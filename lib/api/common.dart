import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:core';
import 'package:new_project/api/error_stream.dart';
import 'package:new_project/constants.dart';
import 'dart:developer' as developer;
import 'dart:convert';
import 'package:new_project/utils/main.dart';
import 'package:new_project/db/user.dart';
import 'package:new_project/db/merchant.dart';

var options = BaseOptions(
  baseUrl: "https://api.hiboss.com.vn",
  connectTimeout: 30000,
  receiveTimeout: 30000,
  validateStatus: (status) {
    return true;
  },
  headers: {},
);

class RequestHeader {
  static String contentType = 'application/json';
  static String accept = 'application/json';
  static String xAuth =
      '8777002bc59f103715b79798082bacd4878f5d53bcfc38eea55e30545a95320c';
  static String xVersion = '3.7.0';
  static String xDataVersion = '1';
  static String xDevice = 'IOS_13.4';
  static String xUniqueDevice = 'a72dc47b560883bd7ed869afec57fcf8';
  static String authorization = '';
  static String xMerchantId = '';
  static String xTenantCode = '';

  static getHeaders() async {
    // return {
    //   'Content-Type': contentType,
    //   // 'Accept': accept,
    //   'X-AUTH': xAuth,
    //   'X-DATA-VERSION': xDataVersion,
    //   'X-VERSION': xVersion,
    //   'X-DEVICE': xDevice,
    //   'X-UNIQUE-DEVICE': xUniqueDevice,
    //   'Authorization': authorization,
    //   'X-MERCHANT-ID': xMerchantId,
    //   'X-TENANT-CODE': xTenantCode,
    //   'X-TIMESTAMP': "1586969049",
    // };
    User userInfo = await UserDB.getUserCached();
    Merchant merchantInfo = await MerchantDB.getMerchantCached();
    return {
      "Accept": "application/json",
      "Authorization":
          userInfo?.accessToken != null ? "Bearer ${userInfo.accessToken}" : "",
      "Content-Type": "application/json",
      "X-DATA-VERSION": "1",
      "X-DEVICE": "IOS_13.4",
      "X-MERCHANT-ID": merchantInfo?.id ?? "",
      "X-TENANT-CODE": merchantInfo?.tenantCode ?? "",
      "X-UNIQUE-DEVICE": "a72dc47b560883bd7ed869afec57fcf8",
      "X-VERSION": "3.7.0"
    };
  }
}

String getUrlWithQueryParam(String url, [Map<String, dynamic> queryParam]) {
  if (queryParam == null) return url;
  String queryString = queryParam.keys
      .map((key) => key + '=' + queryParam[key].toString())
      .toList()
      .join("&");
  return url + '?' + queryString;
}

Future<Response> get(String url, [Map<String, dynamic> queryParam]) async {
  final dio = Dio(options);
  var headers = await RequestHeader.getHeaders();
  var timeStamp =
      (DateTime.now().millisecondsSinceEpoch / 1000).round().toString();
  String xAuthStr = getUrlWithQueryParam(url, queryParam) +
      headers["X-UNIQUE-DEVICE"] +
      headers["X-DATA-VERSION"] +
      headers["X-VERSION"] +
      timeStamp +
      SECRET_KEY;
  String xAuth = getSha256(xAuthStr);
  headers['X-TIMESTAMP'] = timeStamp;
  headers['X-AUTH'] = xAuth;
  dio.options.headers = headers;
  return dio.get(url, queryParameters: queryParam);
}

Future<Response> post(String url, [Map<String, dynamic> data]) async {
  final jsonEncoder = JsonEncoder();
  developer.log("Post Url: " + url);
  developer.log("Post Body: " + jsonEncoder.convert(data).toString());
  final dio = Dio(options);
  var headers = await RequestHeader.getHeaders();
  var timeStamp =
      (DateTime.now().millisecondsSinceEpoch / 1000).round().toString();
  var stringifyBody = jsonEncoder.convert(data).toString();
  var xAuthStr = url +
      headers["X-UNIQUE-DEVICE"] +
      headers["X-DATA-VERSION"] +
      headers["X-VERSION"] +
      timeStamp +
      SECRET_KEY +
      stringifyBody;
  var xAuth = getSha256(xAuthStr);
  headers['X-TIMESTAMP'] = timeStamp;
  headers['X-AUTH'] = xAuth;
  dio.options.headers = headers;
  dio.interceptors.add(
    InterceptorsWrapper(onError: (DioError e) {
      print('onError common: ' + e.message);
      if (e.type == DioErrorType.CONNECT_TIMEOUT ||
          e.type == DioErrorType.RECEIVE_TIMEOUT ||
          e.type == DioErrorType.RECEIVE_TIMEOUT) {
        ApiErrorStream.getInstance().add(GeneralErrors.REQUEST_TIMEOUT);
      }
      return e;
    }, onResponse: (Response response) {
      // Error happen
      if (response.statusCode >= 400 && response.statusCode < 599) {
        ApiErrorStream.getInstance().add(GeneralErrors.INTERNAL_SERVER_ERROR);
      }
      return response;
    }),
  );
  return dio.post(url, data: data);
}
