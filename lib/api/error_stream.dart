import 'dart:async';
import 'dart:core';

class ApiErrorStream {
  static StreamController<String> errorStream;
  static StreamController getInstance(){
    if (errorStream == null){
      errorStream = StreamController();
    }
    return errorStream;
  }

  static reset(){
    if (errorStream != null){
      errorStream.close();
    }
    errorStream = null;
  }
}

