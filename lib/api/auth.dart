import 'common.dart';
import 'package:dio/dio.dart';
import 'dart:async';

Future<Response> signIn(
    {String tenant_code, String userName, String password}) {
  return post('/user-tenant/signin', {
    "tenant_code": tenant_code,
    "userName": userName,
    "password": password,
  });
}
