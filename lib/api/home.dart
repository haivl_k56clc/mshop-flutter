import 'common.dart';
import 'package:dio/dio.dart';
import 'dart:async';

Future<Response> getStatistic() {
  return get('/home/statistical');
}

Future<Response> getActivitiesRecent([page = 1]) {
  return get('/trans-activity/get-trans-activity', {"page": page});
}
